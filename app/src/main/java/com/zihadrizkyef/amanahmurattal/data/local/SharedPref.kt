package com.zihadrizkyef.amanahmurattal.data.local

import android.accounts.Account
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.zihadrizkyef.amanahmurattal.R
import com.zihadrizkyef.amanahmurattal.data.dataclass.CalendarEvent
import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.TrackX
import java.lang.reflect.Type

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 25/07/18.
 */

class SharedPref(private val context: Context, token: String) {
    private val PREF_NAME = context.getString(R.string.app_name) + "-shprf-" + token
    private val KEY_ACCOUNT = "account"
    private val KEY_WISH_LIST = "wishlist"
    private val KEY_EVENT_LIST = "eventlist"

    private val shrPref: SharedPreferences by lazy {context.getSharedPreferences(PREF_NAME, 0)}

    var account: Account?
        get() {
            val json = shrPref.getString(KEY_ACCOUNT, "")!!
            return if (json.isEmpty()) {
                null
            } else {
                Gson().fromJson<Account>(json, Account::class.java)
            }
        }
        set(value) {
            val json = Gson().toJson(value)
            shrPref.edit().putString(KEY_ACCOUNT, json).apply()
        }

    var wishList: ArrayList<TrackX>
        get() {
            val json = shrPref.getString(KEY_WISH_LIST, "")!!
            return if (json.isEmpty()) {
                arrayListOf()
            } else {
                val type: Type = object : TypeToken<ArrayList<TrackX>>() {}.type
                Gson().fromJson<ArrayList<TrackX>>(json, type)
            }
        }
        set(value) {
            val json = Gson().toJson(value)
            shrPref.edit().putString(KEY_WISH_LIST, json).apply()
        }

    var eventList: ArrayList<CalendarEvent>
        get() {
            val json = shrPref.getString(KEY_EVENT_LIST, "")!!
            return if (json.isEmpty()) {
                arrayListOf()
            } else {
                val type: Type = object : TypeToken<ArrayList<CalendarEvent>>() {}.type
                Gson().fromJson<ArrayList<CalendarEvent>>(json, type)
            }
        }
        set(value) {
            val json = Gson().toJson(value)
            shrPref.edit().putString(KEY_EVENT_LIST, json).apply()
        }
}