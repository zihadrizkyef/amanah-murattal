package com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse

data class Body(
    val track_list: List<Track>
)