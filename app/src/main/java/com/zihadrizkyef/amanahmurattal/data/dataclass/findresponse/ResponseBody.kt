package com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse

data class ResponseBody(
    val message: Message
)