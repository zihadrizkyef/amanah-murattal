package com.zihadrizkyef.amanahmurattal.data.dataclass

data class CalendarEvent(
        val day: Int,
        val month: Int,
        val year: Int,
        val hour: Int,
        val minute: Int,
        val name: String,
        val descriction: String
)