package com.zihadrizkyef.amanahmurattal.data.network

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NetworkClient {
    companion object {
        const val API_BASE_URL = "https://api.musixmatch.com/ws/1.1/"
        const val API_KEY = "ba1e703e3919bf3126bc862c29cda3a4"
    }

    private var okHttpClient: OkHttpClient? = null
    val INSTANCE: NetworkMethod by lazy {

        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val retryInterceptor = Interceptor {
            var retry = 0
            val req = it.request()
            var res = it.proceed(req)
            while (!res.isSuccessful && retry < 3) {
                retry++
                res = it.proceed(req)
            }
            res
        }

        okHttpClient = OkHttpClient
            .Builder()
            .addInterceptor(logInterceptor)
            .addInterceptor(retryInterceptor)
            .readTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .client(okHttpClient!!)
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val apiInterface = retrofit.create(NetworkMethod::class.java)
        apiInterface
    }
}