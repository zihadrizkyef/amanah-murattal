package com.zihadrizkyef.amanahmurattal.data.network

import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkMethod {
    @GET("track.search?apikey=" + NetworkClient.API_KEY + "&page_size=30")
    fun findMurattal(@Query("q_artist") query: String): Call<ResponseBody>
}
