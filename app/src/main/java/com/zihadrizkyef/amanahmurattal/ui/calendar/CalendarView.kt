package com.zihadrizkyef.amanahmurattal.ui.calendar

import com.zihadrizkyef.amanahmurattal.data.dataclass.CalendarEvent

interface CalendarView {
    fun highlightEvents(events: ArrayList<CalendarEvent>)
}