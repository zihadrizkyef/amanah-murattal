package com.zihadrizkyef.amanahmurattal.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.zihadrizkyef.amanahmurattal.R
import com.zihadrizkyef.amanahmurattal.ui.login.LoginActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed ({
            val intent = Intent(this, LoginActivity::class.java)

            finish()
            startActivity(intent)
        }, 5000)
    }

    override fun onBackPressed() {}
}
