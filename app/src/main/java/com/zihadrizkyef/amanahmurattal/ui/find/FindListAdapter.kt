package com.zihadrizkyef.amanahmurattal.ui.find

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.zihadrizkyef.amanahmurattal.R
import com.zihadrizkyef.amanahmurattal.data.MyRepo
import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.Track
import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.TrackX

class FindAdapter(private val context: Context, private var list: ArrayList<Track>, private var repo: MyRepo) :
    RecyclerView.Adapter<FindViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): FindViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_find, parent, false)
        return FindViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: FindViewHolder, position: Int) {
        val track = list[position]

        holder.tvId.text = "#${track.track.track_id}"
        holder.tvArtistName.text = track.track.artist_name
        holder.tvTrackName.text = track.track.track_name

        if (repo.wishList.contains(track.track)) {
            holder.btWishList.setImageResource(R.drawable.ic_favorite_red)
        } else {
            holder.btWishList.setImageResource(R.drawable.ic_favorite_black)
        }

        holder.btWishList.setOnClickListener {
            if (repo.wishList.contains(track.track)) {
                val newList = arrayListOf<TrackX>()
                newList.addAll(repo.wishList)
                newList.remove(track.track)
                repo.wishList = newList
                notifyItemChanged(position)
            } else {
                val newList = arrayListOf<TrackX>()
                newList.addAll(repo.wishList)
                newList.add(track.track)
                repo.wishList = newList
                notifyItemChanged(position)
            }
        }
    }
}

class FindViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvId = itemView.findViewById<TextView>(R.id.tvId)
    val tvArtistName = itemView.findViewById<TextView>(R.id.tvDescription)
    val tvTrackName = itemView.findViewById<TextView>(R.id.tvName)
    val btWishList = itemView.findViewById<ImageButton>(R.id.btWishList)
}