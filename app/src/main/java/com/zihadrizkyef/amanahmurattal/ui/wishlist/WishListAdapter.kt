package com.zihadrizkyef.amanahmurattal.ui.wishlist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.zihadrizkyef.amanahmurattal.R
import com.zihadrizkyef.amanahmurattal.data.MyRepo
import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.TrackX

class WishListAdapter(private val context: Context, private var list: ArrayList<TrackX>, private var repo: MyRepo) :
    RecyclerView.Adapter<WishListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): WishListViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_wish, parent, false)
        return WishListViewHolder(view)
    }

    override fun getItemCount(): Int {
        Log.i("AOEU", "size : " + list.size)
        return list.size
    }

    override fun onBindViewHolder(holder: WishListViewHolder, position: Int) {
        val track = list[position]

        holder.tvId.text = "#${track.track_id} pos$position"
        holder.tvArtistName.text = track.artist_name
        holder.tvTrackName.text = track.track_name

        holder.btRemove.setOnClickListener {
            if (repo.wishList.contains(track)) {
                val newList = arrayListOf<TrackX>()
                newList.addAll(repo.wishList)
                newList.remove(track)
                repo.wishList = newList
                list.remove(track)

                notifyItemRemoved(position)
                notifyItemRangeChanged(position, itemCount)
            }
        }
    }
}

class WishListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvId = itemView.findViewById<TextView>(R.id.tvId)
    val tvArtistName = itemView.findViewById<TextView>(R.id.tvDescription)
    val tvTrackName = itemView.findViewById<TextView>(R.id.tvName)
    val btRemove = itemView.findViewById<ImageButton>(R.id.btRemove)
}