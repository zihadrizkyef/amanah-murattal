package com.zihadrizkyef.amanahmurattal.ui.find

import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.Track

interface FindView {
    fun showTrack(trackList: List<Track>)
    fun showFindNoNetwork()
    fun showProgressBar()
    fun hideProgressBar()
}