package com.zihadrizkyef.amanahmurattal.ui.calendar

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.View
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.zihadrizkyef.amanahmurattal.R
import com.zihadrizkyef.amanahmurattal.data.MyRepo
import com.zihadrizkyef.amanahmurattal.data.dataclass.CalendarEvent
import com.zihadrizkyef.amanahmurattal.data.local.SharedPref
import kotlinx.android.synthetic.main.activity_calendar.view.*

class CalendarCustomView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayoutCompat(context, attrs, defStyleAttr), CalendarView {

    private var repo: MyRepo
    private var presenter: CalendarPresenter
    private var eventAdapter: EventAdapter
    private val eventList = arrayListOf<CalendarEvent>()

    init {
        View.inflate(context, R.layout.activity_calendar, this)

        val account = GoogleSignIn.getLastSignedInAccount(context)!!
        val sharedPref = SharedPref(context, account.email!!)
        repo = MyRepo(context, sharedPref, null)
        presenter = CalendarPresenter(repo, this)
        eventAdapter = EventAdapter(context, eventList)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = eventAdapter
        calendarView.selectedDate = CalendarDay.today()
        calendarView.setOnDateChangedListener { widget, date, selected ->
            eventList.clear()
            eventList.addAll(repo.eventList.filter {
                it.year == date.year &&
                        it.month == date.month &&
                        it.day == date.day
            })

            eventAdapter.notifyDataSetChanged()
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        presenter.getEventList()
    }

    override fun highlightEvents(events: ArrayList<CalendarEvent>) {
        val date = calendarView.selectedDate!!
        val filter = events.filter {
            it.year == date.year &&
                    it.month == date.month &&
                    it.day == date.day
        }
        eventList.clear()
        eventList.addAll(filter)
        eventAdapter.notifyDataSetChanged()

        val days = arrayListOf<CalendarDay>()
        events.forEach { days.add(CalendarDay.from(it.year, it.month, it.day)) }

        val todayHighlighter = OneDayDecorator()
        val eventHighlighter = EventDecorator(Color.parseColor("#70FF0000"), days)
        calendarView.removeDecorators()
        calendarView.addDecorators(todayHighlighter, eventHighlighter)
    }

    fun setSelectedDate(day: Int, month: Int, year: Int) {
        calendarView.selectedDate = CalendarDay.from(year, month, day)
    }
}
