package com.zihadrizkyef.amanahmurattal.ui.calendar

import com.zihadrizkyef.amanahmurattal.data.MyRepo

class CalendarPresenter(private val repo: MyRepo, private val view: CalendarView) {
    fun getEventList() {
        view.highlightEvents(repo.eventList)
    }


}