package com.zihadrizkyef.amanahmurattal.ui.wishlist

import com.zihadrizkyef.amanahmurattal.data.MyRepo

class WishListPresenter(private val repo: MyRepo, private val view: WishListView) {
    fun getWishList() {
        view.showTrack(repo.wishList)
    }
}