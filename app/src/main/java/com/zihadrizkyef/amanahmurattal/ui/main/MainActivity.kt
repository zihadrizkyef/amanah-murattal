package com.zihadrizkyef.amanahmurattal.ui.main

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.zihadrizkyef.amanahmurattal.R
import com.zihadrizkyef.amanahmurattal.data.MyRepo
import com.zihadrizkyef.amanahmurattal.data.local.SharedPref
import com.zihadrizkyef.amanahmurattal.ui.addevent.AddEventCustomView
import com.zihadrizkyef.amanahmurattal.ui.addevent.AddEventListener
import com.zihadrizkyef.amanahmurattal.ui.calendar.CalendarCustomView
import com.zihadrizkyef.amanahmurattal.ui.find.FindCustomView
import com.zihadrizkyef.amanahmurattal.ui.login.LoginActivity
import com.zihadrizkyef.amanahmurattal.ui.wishlist.WishlistCustomView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var repo: MyRepo
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    private lateinit var findView: FindCustomView
    private lateinit var wishListView: WishlistCustomView
    private lateinit var calendarView: CalendarCustomView
    private lateinit var AddEventView: AddEventCustomView

    private val onCreateEventListener = object : AddEventListener {
        override fun onCreateSuccess() {
            val calendarItem = navView.menu.getItem(2)
            if (!calendarItem.isChecked) {
                supportActionBar?.title = "Calendar"
                customViewContainer.removeAllViews()
                customViewContainer.addView(calendarView)
                calendarItem.isChecked = true
            }
        }
    }
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navFind -> {
                if (!item.isChecked) {
                    supportActionBar?.title = "Find"
                    customViewContainer.removeAllViews()
                    customViewContainer.addView(findView)
                    item.isChecked = true
                }
            }

            R.id.navWishlist -> {
                if (!item.isChecked) {
                    supportActionBar?.title = "Wishlist"
                    customViewContainer.removeAllViews()
                    customViewContainer.addView(wishListView)
                    item.isChecked = true
                }
            }

            R.id.navCalendar -> {
                if (!item.isChecked) {
                    supportActionBar?.title = "Calendar"
                    customViewContainer.removeAllViews()
                    customViewContainer.addView(calendarView)
                    item.isChecked = true
                }
            }

            R.id.navCreate -> {
                if (!item.isChecked) {
                    supportActionBar?.title = "Create Event"
                    customViewContainer.removeAllViews()
                    customViewContainer.addView(AddEventView)
                    item.isChecked = true
                }
            }

            R.id.navLogout -> {
                mGoogleSignInClient.signOut()
                        .addOnCompleteListener(this) {
                            startActivity(Intent(this, LoginActivity::class.java))
                            finish()
                        }
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val account = GoogleSignIn.getLastSignedInAccount(this)!!
        val sharedPref = SharedPref(this, account.email!!)
        repo = MyRepo(this, sharedPref, null)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        navView.menu.getItem(0).isChecked = true
        supportActionBar?.title = "Find"

        findView = FindCustomView(this)
        wishListView = WishlistCustomView(this)
        calendarView = CalendarCustomView(this)
        AddEventView = AddEventCustomView(this).apply { onCreateEventListener = this@MainActivity.onCreateEventListener }

        if (intent.hasExtra(AddEventCustomView.KEY_EVENT_INDEX)) {
            Log.i("AOEU", "has extra")
            val eventIndex = intent.getIntExtra(AddEventCustomView.KEY_EVENT_INDEX, 1)
            val event = repo.eventList[eventIndex]
            customViewContainer.addView(calendarView)
            calendarView.setSelectedDate(event.day, event.month, event.year)
            intent.removeExtra(AddEventCustomView.KEY_EVENT_INDEX)
        } else {
            customViewContainer.addView(findView)
        }
    }
}
