package com.zihadrizkyef.amanahmurattal.ui.find

import android.app.Activity
import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.zihadrizkyef.amanahmurattal.R
import com.zihadrizkyef.amanahmurattal.data.MyRepo
import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.Track
import com.zihadrizkyef.amanahmurattal.data.local.SharedPref
import com.zihadrizkyef.amanahmurattal.data.network.NetworkClient
import kotlinx.android.synthetic.main.activity_find.view.*


class FindCustomView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayoutCompat(context, attrs, defStyleAttr), FindView {

    private var repo: MyRepo
    private var presenter: FindPresenter
    private var adapter: FindAdapter
    private val list = arrayListOf<Track>()

    init {
        View.inflate(context, R.layout.activity_find, this)

        val account = GoogleSignIn.getLastSignedInAccount(context)!!
        val networkClient = NetworkClient().INSTANCE
        val sharedPref = SharedPref(context, account.email!!)
        repo = MyRepo(context, sharedPref, networkClient)
        presenter = FindPresenter(repo, this)
        adapter = FindAdapter(context, list, repo)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        recyclerView.hasFixedSize()
        presenter.findTrack("Mishary")

        btClear.setOnClickListener {
            if (etSearch.text.isNotBlank()) {
                etSearch.setText("")
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                imm!!.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT)
            }
        }

        etSearch.setOnEditorActionListener { _, _, _ ->
            if (etSearch.text.isNotBlank()) {
                presenter.findTrack(etSearch.text.toString())
                hideKeyboardFrom(context, etSearch)
            }
            true
        }

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (s!!.toString().isNotBlank()) {
                    btClear.setImageResource(R.drawable.ic_clear)
                } else {
                    btClear.setImageResource(R.drawable.ic_search)
                }
            }
        })
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        adapter.notifyDataSetChanged()
    }

    override fun showTrack(trackList: List<Track>) {
        list.clear()
        list.addAll(trackList)
        adapter.notifyDataSetChanged()
    }

    override fun showFindNoNetwork() {
        Snackbar.make(constraintLayout, "No network, please try again", Snackbar.LENGTH_LONG)
                .setAction("Try again") { presenter.findTrack(etSearch.text.toString()) }
                .show()
    }

    override fun showProgressBar() {
        progressBar.animate().scaleY(1F).scaleX(1F).start()
    }

    override fun hideProgressBar() {
        progressBar.animate().scaleY(0F).scaleX(0F).start()
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
