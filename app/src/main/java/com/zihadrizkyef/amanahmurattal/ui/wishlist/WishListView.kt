package com.zihadrizkyef.amanahmurattal.ui.wishlist

import com.zihadrizkyef.amanahmurattal.data.dataclass.findresponse.TrackX

interface WishListView {
    fun showTrack(trackList: ArrayList<TrackX>)
}